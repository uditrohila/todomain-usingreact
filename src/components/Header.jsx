import React, { Component } from "react";


class Header extends Component {
  state = {
      imagesource:false,
  }

  imagetoggle = () =>{
         this.setState({imagesource: !this.state.imagesource})
  }
  render() {
    return (
      <section className="bg-color">
       
        <div className="background-image">
          {/* <img src={this.state.backgroundimg} alt="imge" /> */}
          <img src={!this.state.imagesource?"/images/bg-desktop-dark.jpg":"/images/bg-desktop-light.jpg"} alt="background-img"/>
        </div>

        <div className="main-container">
          <div className="header">
            <div className="title">TODO</div>

            <div className="theme">
              <img onClick={this.imagetoggle} src={this.state.imagesource?"/images/icon-moon.svg":"/images/icon-sun.svg"} alt="toggle-img" />
            </div>
          </div>
        </div>

      </section>
    );
  }
}

export default Header;