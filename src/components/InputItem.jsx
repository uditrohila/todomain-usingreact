import React, { Component } from "react";

class InputItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputText: ""
        }
    }


handleSubmit = (e) => {
    e.preventDefault();
    this.props.onAddTask(this.state.inputText);
    this.setState({inputText: ""})
}

handleChange = (e) => {
    e.preventDefault();
    this.setState({
      inputText: e.target.value,
    });
  };


 
  render() {
    return (
      <div>
        <section>
          <div className="new-todo">
            {/* check mark div  */}
            <div className="check">
              <div className="check-mark"></div>
            </div>

            {/* todo input div */}
            <div className="new-todo-input">
              <form onSubmit={this.handleSubmit} id="todo-input" autoComplete="off">
                <input
                  type="text"
                  name="content"
                  placeholder="Create a new todo...."
                  onChange={this.handleChange}
                  value={this.state.inputText}
                />
              </form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default InputItem;