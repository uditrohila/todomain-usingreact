import React, { Component } from "react";
import InputItem from "./InputItem";
import { v4 as uuidv4 } from "uuid";
import Header from "./Header";

const FILTERS = [
    {
        label:"all",
        value: "all"
    },
    {
        label: "completed",
        value: "completed"
    },
    {
        label: "Active",
        value: "active"
    },
    {
      // label: "clear Completed",
      // value: "clear"
    }
];

class ListItems extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [
        // { text: "meeting", completed: false, id: uuidv4() },
        // { text: "go to gym", completed: false, id: uuidv4() },
        // { text: "some updates", completed: false, id: uuidv4() },
      ],
      activeFilter: "all",
    };
  }

  handleAddTask = (text) => {
    let newTask = {
      text: text,
      id: uuidv4(),
      completed: false,
    };

    //  console.log(newTask);

    // updating the newtask to the state everytime when newtask is created
    this.setState({
      tasks: [newTask, ...this.state.tasks],
    });
  };

  handleRemoveTask = (id) => {
    console.log("removing the item", id);
    this.setState({ tasks: this.state.tasks.filter((t) => t.id !== id) });
  };

  handleTaskToggle = (id) => {
    console.log("toggling", id);

    this.setState({
      tasks: this.state.tasks.map((task) => {
        if (task.id === id) {
          return {
            ...task,
            completed: !task.completed,
          };
        }

        return task;
      }),
    });
  };


  getFilteredTasks = () => {
    const {activeFilter} = this.state;

    if(activeFilter === "all")
    {
        return this.state.tasks;
    }
    else if(activeFilter === "completed"){
        return this.state.tasks.filter((t) => t.completed);
    }
    else if(activeFilter === "active"){
        return this.state.tasks.filter((t) => !t.completed)
    }
    // else if(activeFilter === "clear"){
    //   return this.setState({tasks: this.state.tasks.filter((t) => t.completed === "false")})
    // }

    return [];
}
 handleSetFilter = (filter) => {
     this.setState({
         activeFilter: filter.value,
     })
 }

 handleclearCompleted = () => {
   console.log("clear completed")
   this.setState({ tasks: this.state.tasks.filter((t) => t.completed === false) });
 }

  render() {
    const tasksLength = this.state.tasks.filter((t) => !t.completed).length;
    const TaskToBeDisplayed = this.getFilteredTasks();

    // console.log(this.props.itemcopy);
    return (
      <section>
        <Header/>
        <InputItem onAddTask={this.handleAddTask} />
        <div className="todo-items-container">
          {/* <!-- all items container --> */}
          <div id="todo-items" className="todo-items">
            {TaskToBeDisplayed.map((item) => {
              return (
                <div key={item.id} className="todo-item">
                  <div className="check">
                    <input
                      checked={item.completed}
                      type="checkbox"
                      className="check-mark"
                      name="task-completed"
                      //   onChange={() => {this.handleTaskToggle(item.id)}}
                      onChange={() => {
                        this.handleTaskToggle(item.id);
                      }}
                    />
                  </div>
                  <div
                    className={
                      item.completed ? "todo-text complete" : "todo-text"
                    }
                  >
                    {item.text}
                  </div>
                  <button
                    className="close"
                    onClick={() => this.handleRemoveTask(item.id)}
                  >
                    <img src="/images/icon-cross.svg" alt="imge" />
                  </button>
                </div>
              );
            })}
             <ul
             className="footer-btns"
        >
          {/* <li>show All</li>
          <li>show Completed</li>
          <li>Show Active</li> */}

          {FILTERS.map((filter) => {
              return <li className="filter-btn" onClick={() => this.handleSetFilter(filter)} key={filter.label}>{filter.label}</li>;
          })}
          <li className="filter-btn" onClick={this.handleclearCompleted}>Clear complete</li>
        </ul>
        <p className="task-left">item Left: {tasksLength}</p>
          </div>
        </div>
      </section>
    );
  }
}

export default ListItems;
